from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from .models import Room, gen_anon, gen_room_name, URL_ROOM, URL_USER
from loungeman.lutils.core import messenger, get_keys

_HELM_ID_ = get_keys()['helm_pw']
_STATIC_ROOMS_ = 'loungeman/static/static_rooms'
_srooms = {r.split(':')[0]:r.split(':')[1] for r in open(_STATIC_ROOMS_, 'r').read().strip().split('\n')}

# Create your views here.
def index(request):

    rooms = Room.objects.all()
    context = {'rooms': rooms}

    return render(request, 'loungeman/index.html', context=context)

def helm(request, user_id):

    if user_id != _HELM_ID_:
        return HttpResponseRedirect(reverse('loungeman:index'))

    rooms = Room.objects.all()
    context = {'rooms': rooms}

    return render(request, 'loungeman/helm.html', context=context)

def room_view(request, room_id, user_id=None):

    try:
        r = Room.objects.get(room_id=room_id)
        uname = r.user_set.get(user_id=user_id).user_name if user_id else None
        context = {'room': r, 'uid': user_id, 'uname': uname}
        return render(request, 'loungeman/room.html', context=context)
    except:
        return HttpResponseRedirect(reverse('loungeman:room', args=(_srooms['ERROR'],)))

def adding_user(request, room_id):

    r = Room.objects.get(room_id=room_id)
    user_name = request.POST.get('user_name', '')
    u = r.user_set.create(user_name=user_name if user_name else gen_anon())
    u.set_qr()

    return HttpResponseRedirect(reverse('loungeman:room_user', args=(room_id, u.user_id)))

def adding_room(request):

    room_name = request.POST.get('room_name', '')
    r = Room(room_name=room_name if room_name else gen_room_name())
    r.save()
    r.set_qr()
    r.chat_set.create()
    u = r.user_set.create(user_name='Admin')
    u.set_qr()
    r.room_owner = u.user_id
    r.save()

    return HttpResponseRedirect(reverse('loungeman:room_user', args=(r.room_id, u.user_id)))

def removing_room(request, room_id):

    r = Room.objects.get(room_id=room_id)
    r.clean()
    r.delete()

    return HttpResponseRedirect(reverse('loungeman:helm', args=(_HELM_ID_,)))

def owner_delete_room(request, room_id, user_id):

    r = Room.objects.get(room_id=room_id)
    if r.room_owner == user_id:
        r.clean()
        r.delete()
    else:
        return HttpResponseRedirect(reverse('loungeman:room', args=(room_id,)))

    return HttpResponseRedirect(reverse('loungeman:index'))

def get_qr(request, room_id, user_id=None):

    r = Room.objects.get(room_id=room_id)
    u = r.user_set.get(user_id=user_id) if user_id else None
    p_img = u.user_qr if user_id else r.room_qr

    context = {
        'img': p_img,
        'room': r,
        'user': u,
    }

    return render(request, 'loungeman/getqr.html', context=context)

def send_qr(request, room_id, user_id=None):

    user_name = request.POST.get('user_name', '')
    user_phone = request.POST.get('user_phone', '')
    # user_email = request.POST.get('user_email', '')

    if user_phone and isinstance(user_phone, str) and len(user_phone)==10 and user_phone.isdigit():

        room_name = Room.objects.get(room_id=room_id).room_name

        if user_id:
            msg = "%s, here's your lounge access to %s: %s" % (user_name, room_name, URL_USER % (room_id, user_id))
        else:
            msg = "%s, you've been invited to join lounge %s: %s" % (user_name, room_name, URL_ROOM % room_id)

        messenger(phone=user_phone, msg=msg)

    if not user_id:
        return HttpResponseRedirect(reverse('loungeman:getqr', args=(room_id,)))

    return HttpResponseRedirect(reverse('loungeman:getqr', args=(room_id, user_id)))
