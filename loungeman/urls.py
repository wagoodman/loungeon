from django.conf.urls import url
from . import views
from django.conf import settings
from django.conf.urls.static import static

app_name = 'loungeman'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^helm(?P<user_id>[^/]+)/$', views.helm, name='helm'),
    url(r'^r/(?P<room_id>[^/]+)/$', views.room_view, name='room'),
    url(r'^r/(?P<room_id>[^/]+)/u/(?P<user_id>[^/]+)/$', views.room_view, name='room_user'),
    url(r'^r/(?P<room_id>[^/]+)/addeduser/$', views.adding_user, name='added'),
    url(r'^addroom/$', views.adding_room, name='addedroom'),
    url(r'^removeroom/(?P<room_id>[^/]+)/$', views.removing_room, name='removedroom'),
    url(r'^delroom/(?P<room_id>[^/]+)/u/(?P<user_id>[^/]+)/$', views.owner_delete_room, name='delroom'),
    url(r'^r/(?P<room_id>[^/]+)/qr/$', views.get_qr, name='getqr'),
    url(r'^r/(?P<room_id>[^/]+)/u/(?P<user_id>[^/]+)/qr/$', views.get_qr, name='getqr'),
    url(r'^r/(?P<room_id>[^/]+)/sendqr/$', views.send_qr, name='sendqr_room'),
    url(r'^r/(?P<room_id>[^/]+)/u/(?P<user_id>[^/]+)/sendqr/$', views.send_qr, name='sendqr_user'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
