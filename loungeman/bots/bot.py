#!/usr/bin/env python3

from loungeman.lutils.core import messenger
from loungeman.bots import notifiers

class Bot:

    def __init__(self, phone):

        self.bot_call = '#bot-%s:'

        self.phone = phone

    def notify(self, msg=''):

        messenger(phone=self.phone, msg=msg)

        return

    def processor(self, content, *args, **kwargs):
        return None

class BotManager:

    def __init__(self, phone):
        self.phone = phone
        self.bots = []

    def add_bot(self, bot):

        self.bots.append(bot)

        return

    def trigger(self, content=''):

        if content.startswith('#bot-'):
            if content[5:].startswith('kw:'):
                kwstr = content.split('bot-kw:')[1]
                self.add_bot(notifiers.KWbot(self.phone).setkws(kwstr))
        else:
            for bot in self.bots:
                res = bot.processor(content)
                if res and isinstance(res, str):
                    bot.notify(msg=res)

        return