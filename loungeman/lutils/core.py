#!/usr/bin/env python3

import os
from twilio.rest import Client

def get_keys():

    p_data = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../../loungeon_data'))

    return {x.split(':::')[0]:x.split(':::')[1] for x in open(p_data, 'r').read().strip().split('\n')}

__keys__ = get_keys()

def messenger(phone='', msg=''):

    if not phone or not msg:
        return False

    client = Client(__keys__['twilio_acc_id'], __keys__['twilio_tok'])

    _ = client.messages.create(
        body=msg,
        from_=__keys__['twilio_phone'],
        to='+1%s' % phone,
    )

    return True
