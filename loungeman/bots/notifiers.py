#!/usr/bin/env python3

from bot import Bot

# Classes
class KWbot(Bot):

    def __init__(self, phone):
        super().__init__(phone=phone)
        self.bot_call = self.bot_call % 'kw'
        self.kws = []

    def setkws(self, kws=None):

        self.kws = [x.strip() for x in kws.strip().split(',')]

        return self

    def processor(self, content, *args, **kwargs):

        matches = [kw for kw in self.kws if kw in content]

        if matches:
             msg = ','.join(matches)
             return msg
        else:
            return None
