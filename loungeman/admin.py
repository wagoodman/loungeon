from django.contrib import admin
from .models import Room, User, Chat

# Register your models here.
admin.site.register(Room)
admin.site.register(User)
admin.site.register(Chat)
