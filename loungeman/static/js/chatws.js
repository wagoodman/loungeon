var chatSocket = new WebSocket(
    'ws://' + window.location.host +
    '/ws/loungeman/' + url0);

chatSocket.onopen = function(e) {
    chatSocket.send(JSON.stringify({
        'userid': userid,
    }));
}

chatSocket.onmessage = function(e) {
    var data = JSON.parse(e.data);
    var message = data['message'];
    var users = data['users'];
    if (message) {
        chatarea = document.querySelector('#chatlog');
        chatarea.value += (message + '\n');
        chatarea.scrollTop = chatarea.scrollHeight;
    }
    if (users) {
        document.querySelector('#userlog').value = users;
    }
};

chatSocket.onclose = function(e) {
    console.error('Chat socket closed unexpectedly');
};

document.querySelector('#comment').onkeyup = function(e) {
    if (e.keyCode === 13) {  // enter, return
        document.querySelector('#send').click();
    }
};

document.querySelector('#send').onclick = function(e) {
    var messageInputDom = document.querySelector('#comment');
    var message = messageInputDom.value;
    chatSocket.send(JSON.stringify({
        'message': message,
    }));
    messageInputDom.value = '';
};

document.querySelector('#leave').onclick = function(e) {
    chatSocket.send(JSON.stringify({
        'exit': '',
    }));
    window.location.replace('//' + window.location.host + '/' + urlroom);
};
