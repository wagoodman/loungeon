import string
import qrcode
from django.db import models
from numpy import random
import os
from django.conf import settings

# Constants
ID_LEN = 40
ID_CHARS = list(string.ascii_letters + string.digits)
COM_SEP = '/'
COM_UC = ':'
URL_IP = 'http://www.loungeon.com'
URL_ROOM = URL_IP + '/r/%s/'
URL_USER = URL_IP + '/r/%s/u/%s/'

# Functions
def _gen_id():
    return ''.join(random.choice(ID_CHARS, ID_LEN))

def gen_anon():
    return 'anon' + ''.join(random.choice(list(string.digits), 5))

def gen_room_name():
    return 'lounge' + ''.join(random.choice(list(string.digits), 5))

def to_ords(text):
    return ' '.join(str(ord(i)) for i in text)

def to_str(ordtext):
    return ''.join([chr(int(i)) for i in ordtext.split()])

def gen_qr(val):
    qr = qrcode.QRCode(version=1, error_correction=qrcode.constants.ERROR_CORRECT_H, box_size=10, border=4)
    qr.add_data(val)
    qr.make(fit=True)
    img = qr.make_image()
    p_qr = _gen_id() + '.jpg'
    img.save(settings.MEDIA_URL + p_qr)
    return p_qr

# Models
class Room(models.Model):
    room_name = models.CharField(max_length=40, default='')
    room_id = models.CharField(max_length=ID_LEN, default=_gen_id, unique=True)
    room_max_size = models.IntegerField(default=0)
    room_owner = models.CharField(max_length=ID_LEN, default='')
    room_qr = models.CharField(max_length=100, default='')

    def __str__(self):
        return self.room_name

    def get_user_ids(self):
        return [u.user_id for u in self.user_set.all()]

    def get_user_names(self, exclude=None):
        names = [u.user_name for u in self.user_set.all()]
        if exclude and exclude in names:
            names.remove(exclude)
        return '\n'.join(names)

    def set_qr(self):
        self.room_qr = gen_qr(URL_ROOM % self.room_id)
        self.save()
        return

    def clean(self):
        self.chat_set.all().delete()
        for u in self.user_set.all():
            u.del_qr()
        self.user_set.all().delete()
        os.remove(settings.MEDIA_URL + self.room_qr)
        return

class User(models.Model):
    room = models.ForeignKey(Room, null=True, on_delete=models.SET_NULL)
    user_name = models.CharField(max_length=100, default='')
    user_id = models.CharField(max_length=ID_LEN, default=_gen_id, unique=True)
    user_phone = models.CharField(max_length=10, default='')
    user_email = models.CharField(max_length=100, default='')
    user_qr = models.CharField(max_length=100, default='')

    def __str__(self):
        return self.user_name

    def set_qr(self):
        self.user_qr = gen_qr(URL_USER % (self.room.room_id, self.user_id))
        self.save()
        return

    def del_qr(self):
        os.remove(settings.MEDIA_URL + self.user_qr)
        return

class Chat(models.Model):
    room = models.ForeignKey(Room, null=True, on_delete=models.SET_NULL)
    chat_id = models.CharField(max_length=ID_LEN, default=_gen_id, unique=True)
    chatter = models.TextField(default='', blank=True)
    user_ids = models.TextField(default='', blank=True)

    def get_user_ids(self):
        if self.user_ids:
            return self.user_ids.split('/')
        else:
            return []

    def get_chatter(self, n=20):
        chats = []
        if self.chatter:
            for x in self.chatter.split(COM_SEP)[-n:]:
                u, c = x.split(COM_UC)
                chats.append(self.make_message(to_str(u), to_str(c)))

        return '\n'.join(chats)

    @staticmethod
    def make_message(user_name, comment):
        return '%s:  %s' % (user_name, comment)

    def add_comment(self, user_name, comment):
        if self.chatter:
            self.chatter += COM_SEP + to_ords(user_name) + COM_UC + to_ords(comment)
        else:
            self.chatter += to_ords(user_name) + COM_UC + to_ords(comment)
        self.save()
        return
