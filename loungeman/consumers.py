# loungeman/consumers.py
from channels.generic.websocket import AsyncWebsocketConsumer
from .models import Room, User, Chat
import json

_NO_ONE_HERE_ = '<No One Right Now>'

class ChatConsumer(AsyncWebsocketConsumer):

    async def connect(self):
        self.room_id = self.scope['url_route']['kwargs']['room_id']
        self.user_id = self.scope['url_route']['kwargs']['user_id'] if 'user_id' in self.scope['url_route']['kwargs'] else ''
        self.room = Room.objects.get(room_id=self.room_id)
        self.uname = self.room.user_set.get(user_id=self.user_id).user_name if self.user_id else None
        self.room_group_id = 'chat_%s' % self.room_id

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_id,
            self.channel_name
        )

        await self.accept()

        # Show previous messages and current users
        unames = self.room.get_user_names()
        await self.send(text_data=json.dumps({
            'message': self.room.chat_set.all()[0].get_chatter(),
            'users': unames if unames else _NO_ONE_HERE_,
        }))

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_id,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message'] if 'message' in text_data_json else None
        newuserid = text_data_json['userid'] if 'userid' in text_data_json else None
        leave = True if 'exit' in text_data_json else False

        if message:
            self.room.chat_set.all()[0].add_comment(self.uname, message)
            # Send message to room group
            await self.channel_layer.group_send(
                self.room_group_id,
                {
                    'type': 'chat.message',
                    'message': Chat.make_message(self.uname, message),
                }
            )

        if newuserid:
            await self.channel_layer.group_send(
                self.room_group_id,
                {
                    'type': 'chat.users',
                    'users': self.room.get_user_names(),
                }
            )

        if leave:
            u = User.objects.get(user_id=self.user_id)
            u.del_qr()
            u.delete()
            await self.channel_layer.group_send(
                self.room_group_id,
                {
                    'type': 'chat.users',
                    'users': self.room.get_user_names(),
                }
            )

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message'] if 'message' in event else None

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message if message else None,
        }))

    async def chat_users(self, event):
        users = event['users'] if 'users' in event else None

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'users': users if users else _NO_ONE_HERE_,
        }))
