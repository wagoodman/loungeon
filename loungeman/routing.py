# loungeman/routing.py
from django.conf.urls import url
from . import consumers

websocket_urlpatterns = [
    url(r'^ws/loungeman/r/(?P<room_id>[^/]+)/u/(?P<user_id>[^/]+)/$', consumers.ChatConsumer),
    url(r'^ws/loungeman/r/(?P<room_id>[^/]+)/$', consumers.ChatConsumer),
]
